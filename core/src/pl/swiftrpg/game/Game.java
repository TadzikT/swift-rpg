package pl.swiftrpg.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.gui.GUIStage;
import pl.swiftrpg.game.world.WorldStage;

public class Game extends ApplicationAdapter {
    private static Game ourInstance = new Game();

	private SpriteBatch batch;
	private OrthographicCamera guiCamera;
    private OrthographicCamera worldCamera;
	private ExtendViewport guiViewport;
    private ExtendViewport worldViewport;
	private Skin skin;
	private ShapeRenderer shapeRenderer;
    private GUIStage guiStage;
    private WorldStage worldStage;

	@Override
	public void create () {
	    Assets.loadAssets();
        batch = new SpriteBatch();
        guiCamera = new OrthographicCamera();
        worldCamera = new OrthographicCamera();
        guiViewport = new ExtendViewport(Consts.TARGET_WIDTH, Consts.TARGET_HEIGHT, guiCamera);
        worldViewport = new ExtendViewport(Consts.TARGET_WIDTH, Consts.TARGET_HEIGHT, worldCamera);
        skin = new Skin(Gdx.files.internal("gui.json"));
        Texture.setAssetManager(Assets.getManager());
        shapeRenderer = new ShapeRenderer();
        while (!Assets.getManager().update()) {
            // waiting for assets to be loaded
        }
        worldStage = new WorldStage(worldViewport, batch);
        guiStage = new GUIStage(guiViewport, batch);
        Gdx.input.setInputProcessor(new InputMultiplexer(guiStage, worldStage));
	}

	@Override
	public void render () {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT
                | (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));
        Gdx.gl.glClearColor(90/255f, 155/255f, 119/255f, 1);
        worldStage.act(Gdx.graphics.getDeltaTime());
        worldStage.draw();
        guiStage.act(Gdx.graphics.getDeltaTime());
        guiStage.draw();
	}

    @Override
    public void resize (int width, int height) {
        guiViewport.update(width, height);
        worldViewport.update(width, height);
    }

	@Override
	public void dispose () {
		batch.dispose();
		skin.dispose();
		guiStage.dispose();
		worldStage.dispose();
	}

    public OrthographicCamera getGuiCamera() {
        return guiCamera;
    }

    public OrthographicCamera getWorldCamera() {
        return worldCamera;
    }

    public Skin getSkin() {
        return skin;
    }

    public ShapeRenderer getShapeRenderer() {
        return shapeRenderer;
    }

    public static Game getInstance() {
        return ourInstance;
    }

    public WorldStage getWorldStage() {
        return worldStage;
    }
}

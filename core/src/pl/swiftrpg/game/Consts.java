package pl.swiftrpg.game;

public final class Consts {
    public final static int TARGET_WIDTH = 1920;
    public final static int TARGET_HEIGHT = 1080;
    public final static int WINDOWED_WIDTH = 1280;
    public final static int WINDOWED_HEIGHT = 720;
    public final static int MSAA_SAMPLES = 16;
    public final static String GAME_NAME = "Swift RPG";
    public final static String DESKTOP_ICON_PATH = "ic_launcher.png";
    public final static int SMALL_FONT_SIZE = 24;
    public final static int BIG_FONT_SIZE = 27;
    public final static float BOX_TO_WORLD = 50f;
}
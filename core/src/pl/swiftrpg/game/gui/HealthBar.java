package pl.swiftrpg.game.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import pl.swiftrpg.game.Game;
import pl.swiftrpg.game.entities.world.Player;
import pl.swiftrpg.game.utils.Utils;

public class HealthBar extends Actor {
    private static Texture healthBar = Utils.createTexture(Game.getInstance().getSkin().getColor("red"));
    private static Texture healthBarBackground =
            Utils.createTexture(Game.getInstance().getSkin().getColor("blackish"));
    private static float healthBarBorder = 7;
    private Player player = Game.getInstance().getWorldStage().getPlayer();

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        float healthBarWidth = Utils.map(player.getHp(), 0, player.getMaxHp(), 0,
                300 + healthBarBorder);
        batch.draw(healthBarBackground, 20, 20, 320,40 + healthBarBorder * 2);
        batch.draw(healthBar, 20 + healthBarBorder, 20 + healthBarBorder, healthBarWidth, 40);
    }
}

package pl.swiftrpg.game.gui.buttons;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import pl.swiftrpg.game.Game;
import pl.swiftrpg.game.gui.inventory.Inventory;

public class InventoryButton extends Button {
    private Inventory inventory;

    public InventoryButton(final Inventory inventory) {
        super();
        this.inventory = inventory;
        setStyle(Game.getInstance().getSkin().get(Button.ButtonStyle.class));
        setBounds(1000, 0, 120, 120);
        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                toggleAction();
                return true;
            }
        });
    }
    private void toggleAction() {
        inventory.setVisible(!inventory.isVisible());
    }

    @Override
    public void toggle() {
        super.toggle();
        toggleAction();
    }
}

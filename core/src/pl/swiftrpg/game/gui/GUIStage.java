package pl.swiftrpg.game.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import pl.swiftrpg.game.Consts;

public class GUIStage extends Stage {
    private GUI gui = GUI.getInstance();

    public GUIStage(Viewport viewport, SpriteBatch batch) {
        super(viewport, batch);
        addActor(gui);
        setKeyboardFocus(gui);
        addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                switch (keycode) {
                    case Keys.ENTER:
                        if (Gdx.input.isKeyPressed(Keys.ALT_LEFT) || Gdx.input.isKeyPressed(Keys.ALT_RIGHT)) {
                            if (Gdx.graphics.isFullscreen()) {
                                Gdx.graphics.setWindowedMode(Consts.WINDOWED_WIDTH, Consts.WINDOWED_HEIGHT);
                            } else {
                                Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
                            }
                        }
                        break;
                    case Keys.F4:
                        if (Gdx.input.isKeyPressed(Keys.ALT_LEFT)) {
                            Gdx.app.exit();
                        }
                        break;
                }
                return false;
            }
        });
    }

}

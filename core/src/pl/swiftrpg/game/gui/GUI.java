package pl.swiftrpg.game.gui;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import pl.swiftrpg.game.entities.items.EquippableItemGenerator;
import pl.swiftrpg.game.entities.items.Item;
import pl.swiftrpg.game.entities.items.ItemRarity;
import pl.swiftrpg.game.entities.items.StackableItem;
import pl.swiftrpg.game.gui.buttons.InventoryButton;
import pl.swiftrpg.game.gui.inventory.Inventory;
import pl.swiftrpg.game.gui.inventory.InventorySystem;

public class GUI extends Group {
    private static GUI ourInstance = new GUI();

    private InventorySystem inventorySystem;
    private InventoryButton inventoryButton;
    private HealthBar healthBar;

    private GUI() {
        inventorySystem = new InventorySystem();
        inventoryButton = new InventoryButton(inventorySystem.getInventory());
        healthBar = new HealthBar();
        addActor(inventoryButton);
        addActor(inventorySystem);
        addActor(healthBar);
        addListener(new InputListener(){
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                Inventory inventory = inventorySystem.getInventory();
                switch (keycode) {
                    case Input.Keys.E:
                        inventoryButton.toggle();
                        break;
                    case Input.Keys.R:
                        inventory.getEquipment().addItem(new Item(ItemRarity.WHITE, "itemek", 5, 20));
                        inventorySystem.manualUpdateCursor();
                        break;
                    case Input.Keys.T:
                        inventory.getEquipment().addItem(EquippableItemGenerator.generateEquippableItem(1));
                        inventorySystem.manualUpdateCursor();
                        break;
                    case Input.Keys.NUM_1:
                        inventory.getEquipment().addItem(new StackableItem(ItemRarity.GREEN, "Cheese", 1, 10, 1));
                        inventorySystem.manualUpdateCursor();
                        break;
                    case Input.Keys.NUM_2:
                        inventory.getEquipment().addItem(new StackableItem(ItemRarity.GREEN, "Cheese", 1, 10, 10));
                        inventorySystem.manualUpdateCursor();
                        break;
                    case Input.Keys.NUM_3:
                        inventory.getEquipment().addItem(new StackableItem(ItemRarity.GREEN, "Cheese", 1, 10, 30));
                        inventorySystem.manualUpdateCursor();
                        break;
                }
                return false;
            }
        });
    }

    public InventorySystem getInventorySystem() {
        return inventorySystem;
    }

    public static GUI getInstance() {
        return ourInstance;
    }

}


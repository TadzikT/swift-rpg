package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.Game;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.assets.LazyBitmapFontController;
import pl.swiftrpg.game.entities.items.Attribute;
import pl.swiftrpg.game.entities.items.EquippableItem;
import pl.swiftrpg.game.entities.items.Item;

import java.util.Map;

public class ItemTooltip extends Table {
    private static ShapeRenderer shapeRenderer = Game.getInstance().getShapeRenderer();
    private static Camera camera = Game.getInstance().getGuiCamera();
    private LazyBitmapFontController font = Assets.getFont();
    private Item item;
    private int offsetX;
    private boolean isItCompareTooltip;
    private int smallFont = Consts.SMALL_FONT_SIZE;
    private int bigFont = Consts.BIG_FONT_SIZE;

    public ItemTooltip(boolean isItCompareTooltip) {
        super(Game.getInstance().getSkin());
        this.isItCompareTooltip = isItCompareTooltip;
        offsetX = isItCompareTooltip ? 6 : 50;
        pad(15);
        setVisible(false);
        // Useful for Android only.
        setTouchable(Touchable.disabled);
    }

    public void setItem(AbstractItemSlot itemSlot) {
        if (itemSlot == null || itemSlot.getItem() == null) {
            return;
        }
        item = itemSlot.getItem();
        clearChildren();

        if (isItCompareTooltip) {
            add(font.getLabel("Currently equipped:", smallFont, "dark-grey")).left().row();
        }
        add(font.getLabel(item.getName(), bigFont, item.getItemRarity().getColor())).left().row();
        if (item instanceof EquippableItem) {
            EquippableItem equippableItem = (EquippableItem)item;
            add(font.getLabel(equippableItem.getEquippableSlot().getName(), smallFont, "white")).left().row();
            for (Map.Entry<Attribute, Integer> entry: equippableItem.getAttributes().entrySet()) {
                add(font.getLabel("+" + entry.getValue() + " " + entry.getKey().getName(), smallFont,
                        "green")).left().row();
            }
            add(font.getLabel("Requires level " + item.getLevel() + " to equip", smallFont, "white")).left()
                    .row();
        } else {
            add(font.getLabel("Requires level " + item.getLevel() + " to equip", smallFont, "white")).left()
                    .row();
        }
        add(font.getLabel("Sell price: " + item.getTotalPrice(itemSlot.getContainer().getItemPriceMultiplier())
                + "g", smallFont, "gold")).left().row();
        pack();
        setVisible(true);
    }

    public void clearIt() {
        item = null;
        setVisible(false);
    }

    @Override
    public void draw (Batch batch, float parentAlpha) {
        batch.end();
        shapeRenderer.setProjectionMatrix(camera.combined);
        Gdx.gl.glEnable(GL20.GL_BLEND);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(getSkin().getColor("bluish-grey"));
        // Those two "- 1" are because this rectangle's last vertical and horizontal line will be occluded by a Line.
        shapeRenderer.rect(getX(), getY(), getWidth() - 1, getHeight() - 1);
        shapeRenderer.end();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(getSkin().getColor(item.getItemRarity().getColor()));
        shapeRenderer.rect(getX(), getY(), getWidth(), getHeight());
        shapeRenderer.end();
        batch.begin();
        super.draw(batch, parentAlpha);
    }

    public void setItemTooltipPosition(float x, float y) {
        if (!isVisible()) {
            return;
        }
        float xPos;
        // True if cursor is on the right side of the screen. Otherwise false.
        if (x >= (Consts.TARGET_WIDTH / 2)) {
            xPos = x - getWidth() - offsetX;
        } else {
            xPos = x + offsetX;
        }
        float yPos = y - (getHeight() / 2);
        setX(xPos);
        keepInScreen(yPos);
    }

    public void setCompareItemTooltipPosition(float x, ItemTooltip itemTooltip) {
        if (!isVisible()) {
            return;
        }
        float xPos;
        // True if cursor is on the right side of the screen. Otherwise false.
        if (x >= (Consts.TARGET_WIDTH / 2)) {
            xPos = itemTooltip.getX() - getWidth() - offsetX;
        } else {
            xPos = itemTooltip.getX() + getWidth() + offsetX;
        }
        float yPos = itemTooltip.getTop() - getHeight() + itemTooltip.getRowHeight(0);
        setX(xPos);
        keepInScreen(yPos);
    }

    private void keepInScreen(float yPos) {
        if (yPos + getHeight() >= Consts.TARGET_HEIGHT - 1) {
            setY(Consts.TARGET_HEIGHT - 1 - getHeight());
        } else if (yPos <= 1) {
            setY(1);
        } else {
            setY(yPos);
        }
    }
}

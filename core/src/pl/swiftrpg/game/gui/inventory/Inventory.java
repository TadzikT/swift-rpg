package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.entities.items.EquippableItemGenerator;
import pl.swiftrpg.game.entities.items.ItemRarity;
import pl.swiftrpg.game.entities.items.StackableItem;

public class Inventory extends AbstractInventoryPanel {
    private Equipment equipment;
    private PaperDoll paperDoll;
    private Label goldLabel;
    private int gold = 1000;

    public Inventory(int slotSize) {
        super();
        setBounds(Consts.TARGET_WIDTH - 800, 0, 800, Consts.TARGET_HEIGHT);
        equipment = new Equipment(slotSize, this, 24);
        paperDoll = new PaperDoll(slotSize, this, (int)getHeight());
        goldLabel = Assets.getFont().getLabel("Gold: " + getGold() + "g", Consts.SMALL_FONT_SIZE, "white");
        goldLabel.setPosition(getWidth() - 300, getHeight() / 2);
        addActor(equipment);
        addActor(paperDoll);
        addActor(goldLabel);

        for (int i = 0; i < 8; i++) {
            equipment.addItem(EquippableItemGenerator.generateEquippableItem(1));
        }
        equipment.addItem(new StackableItem(ItemRarity.GREEN, "Cheese", 1, 10, 1));
        equipment.addItem(new StackableItem(ItemRarity.GREEN, "Rat tail", 1, 10, 4));
        equipment.addItem(new StackableItem(ItemRarity.GREEN, "Rat fur", 1, 10, 18));
        equipment.addItem(new StackableItem(ItemRarity.GREEN, "Rat fur", 1, 10, 25));
        equipment.addItem(new StackableItem(ItemRarity.BLUE, "Rat foot", 1, 10, 12));
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public PaperDoll getPaperDoll() {
        return paperDoll;
    }

    private void updateGoldLabel() {
        goldLabel.setText("Gold: " + getGold() + "g");
    }

    public int getGold() {
        return gold;
    }

    public void addGold(int gold) {
        this.gold += gold;
        updateGoldLabel();
    }

    public boolean takeGold(int gold) {
        if (gold > this.gold) {
            return false;
        } else {
            this.gold -= gold;
            updateGoldLabel();
            return true;
        }
    }
}

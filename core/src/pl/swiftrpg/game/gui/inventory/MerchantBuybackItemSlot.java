package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.math.GridPoint2;
import pl.swiftrpg.game.entities.items.Item;
import pl.swiftrpg.game.gui.GUI;

public class MerchantBuybackItemSlot extends AbstractItemSlot {

    public MerchantBuybackItemSlot(GridPoint2 position, int slotSize, AbstractItemSlotsContainer parent) {
        super(position, slotSize, parent);
    }

    @Override
    public void swapItems(AbstractItemSlot sourceSlot) {
        if (sourceSlot.getItem() == getItem()
                || !(isThisItemValidHere(sourceSlot.getItem()) && sourceSlot.isThisItemValidHere(getItem()))) {
            return;
        }
        if (sourceSlot instanceof MerchantBuyItemSlot || sourceSlot instanceof MerchantBuybackItemSlot) {
            return;
        }

        Inventory inventory = GUI.getInstance().getInventorySystem().getInventory();
        inventory.addGold(sourceSlot.getItem().getTotalPrice(sourceSlot.getContainer().getItemPriceMultiplier()));
        ((Merchant)getContainer().getContainer()).getMerchantBuyback().addItem(sourceSlot.getItem());
        sourceSlot.setItem(null);
    }

}


package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.math.GridPoint2;
import pl.swiftrpg.game.entities.items.EquippableItem;
import pl.swiftrpg.game.entities.items.EquippableSlot;
import pl.swiftrpg.game.entities.items.Item;
import pl.swiftrpg.game.gui.GUI;
import pl.swiftrpg.game.utils.Utils;

public class PaperDollItemSlot extends AbstractItemSlot {
    private EquippableSlot equippableSlot;

    public PaperDollItemSlot(GridPoint2 position, int slotSize, AbstractItemSlotsContainer parent, EquippableSlot equippableSlot) {
        super(position, slotSize, parent);
        this.equippableSlot = equippableSlot;
    }

    @Override
    public boolean isThisItemValidHere(Item item) {
        return (item == null) ||
                ((item instanceof EquippableItem) && (((EquippableItem)item).getEquippableSlot() == equippableSlot));
    }

    @Override
    public void setItem(Item item) {
        super.setItem(item);
        Utils.log("An item in paper doll has changed. Character stats should be updated now.");
    }

    @Override
    public void swapItems(AbstractItemSlot sourceSlot) {
        if (sourceSlot.getItem() == getItem()
                || !(isThisItemValidHere(sourceSlot.getItem()) && sourceSlot.isThisItemValidHere(getItem()))) {
            return;
        }

        Inventory inventory = GUI.getInstance().getInventorySystem().getInventory();
        if (sourceSlot instanceof MerchantBuyItemSlot || sourceSlot instanceof MerchantBuybackItemSlot) {
            if (getItem() == null && inventory.takeGold(sourceSlot.getItem()
                    .getTotalPrice(sourceSlot.getContainer().getItemPriceMultiplier()))) {
                if (sourceSlot instanceof MerchantBuyItemSlot) {
                    setItem(sourceSlot.getItem());
                    sourceSlot.setItem(null);
                // handling MerchantBuybackItemSlot
                } else {
                    setItem(sourceSlot.getItem());
                    sourceSlot.setItem(null);
                    MerchantBuyback merchantBuyback = (MerchantBuyback) sourceSlot.getContainer();
                    merchantBuyback.order(merchantBuyback.getItemSlots().indexOf(sourceSlot));
                }
            }
        } else {
            Item tempItem = getItem();
            setItem(sourceSlot.getItem());
            sourceSlot.setItem(tempItem);
        }
    }

    @Override
    public AbstractItemSlot getCompareItemSlot() {
        return null;
    }

    public EquippableSlot getEquippableSlot() {
        return equippableSlot;
    }
}


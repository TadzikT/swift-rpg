package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import pl.swiftrpg.game.Game;

import static com.badlogic.gdx.Application.ApplicationType.Android;
import static com.badlogic.gdx.Application.ApplicationType.Desktop;

public class InventorySystemListener extends InputListener {
    private static Skin skin = Game.getInstance().getSkin();
    private Group parent;
    private ItemTooltip itemTooltip;
    private ItemTooltip compareTooltip;
    private DraggedItemIcon draggedItemIcon;
    private AbstractItemSlot hoveredItemSlot;
    private AbstractItemSlot touchedItemSlot;
    private AbstractItemSlot draggedItemSlot = null;
    private Vector2 touchDownPoint = new Vector2();
    private float dragDistance;

    public InventorySystemListener(Group parent, ItemTooltip itemTooltip, ItemTooltip compareTooltip,
                                   DraggedItemIcon draggedItemIcon) {
        this.parent = parent;
        this.itemTooltip = itemTooltip;
        this.compareTooltip = compareTooltip;
        this.draggedItemIcon = draggedItemIcon;
        if (Gdx.app.getType() == Desktop) {
            dragDistance = 0;
        } else if (Gdx.app.getType() == Android) {
            dragDistance = 10;
        }
    }

    @Override
    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
        if (Gdx.app.getType() == Android && pointer != 0) {
            return;
        }
        if (Gdx.app.getType() == Desktop && pointer != -1) {
            return;
        }
        updateCursor(event.getTarget(), x, y, false);
        if (draggedItemSlot != null) {
            dragItemSlotValidation(x, y);
            draggedItemIcon.update(x, y);
        }
    }

    @Override
    public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        if (Gdx.app.getType() == Android && pointer != 0) {
            return;
        }
        if (Gdx.app.getType() == Desktop && pointer != -1) {
            return;
        }
        if (event.getTarget() instanceof AbstractItemSlot) {
            AbstractItemSlot itemSlot = (AbstractItemSlot)event.getTarget();
            itemSlot.setColor(skin.getColor("default"));
            itemTooltip.clearIt();
            compareTooltip.clearIt();
        }
    }

    @Override
    public boolean mouseMoved(InputEvent event, float x, float y) {
        updateCursor(event.getTarget(), x, y, false);
        return true;
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        if (Gdx.app.getType() == Android && pointer != 0) {
            return;
        }
        boolean pressed = false;
        if (event.getTarget() instanceof AbstractItemSlot) {
            AbstractItemSlot itemSlot = ((AbstractItemSlot)event.getTarget());
            pressed = itemSlot.getItem() != null && itemSlot == hoveredItemSlot
                    && Gdx.input.isButtonPressed(Input.Buttons.LEFT);
        }
        updateCursor(parent.hit(x, y, true), x, y, pressed);
        if (event.getTarget() instanceof AbstractItemSlot) {
            if (draggedItemSlot == null && hoveredItemSlot == touchedItemSlot
                    && isOutOfDistance(new Vector2(x, y), touchDownPoint, dragDistance)) {
                draggedItemSlot = touchedItemSlot;
                Gdx.input.vibrate(100);
                draggedItemIcon.setItemSlot(draggedItemSlot);
            }
            if (draggedItemSlot != null) {
                dragItemSlotValidation(x, y);
                draggedItemIcon.update(x, y);
            }
        }
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        if (Gdx.app.getType() == Android && pointer != 0) {
            return true;
        }
        if (event.getTarget() instanceof AbstractItemSlot) {
            AbstractItemSlot itemSlot = (AbstractItemSlot)event.getTarget();
            if (itemSlot.getItem() != null) {
                if (button == Input.Buttons.LEFT) {
                    touchedItemSlot = itemSlot;
                    touchedItemSlot.setColor(skin.getColor("grey"));
                    touchDownPoint.set(x, y);
                } else if (button == Input.Buttons.RIGHT && draggedItemSlot == null) {
                    touchedItemSlot = null;
                    AbstractItemSlot compareItemSlot = hoveredItemSlot.getCompareItemSlot();
                    if (hoveredItemSlot.getCompareItemSlot() != null) {
                        // fast swap works only with item slots in equipment and stash
                        if (hoveredItemSlot instanceof ItemSlot) {
                            hoveredItemSlot.swapItems(compareItemSlot);
                            updateCursor(hoveredItemSlot, x, y, false);
                        }
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
        if (Gdx.app.getType() == Android && pointer != 0) {
            return;
        }
        if (button == Input.Buttons.LEFT) {
            if (event.getTarget() instanceof AbstractItemSlot) {
                AbstractItemSlot itemSlot = (AbstractItemSlot)event.getTarget();
                itemSlot.setColor(skin.getColor
                        (parent.hit(x, y, true) == itemSlot ? "light-grey" : "default"));
            }
            if (draggedItemSlot != null) {
                Gdx.input.vibrate(100);
                Actor underCursor = parent.hit(x, y, true);
                if (underCursor instanceof AbstractItemSlot) {
                    AbstractItemSlot targetSlot = (AbstractItemSlot)underCursor;
                    targetSlot.swapItems(draggedItemSlot);
                    updateCursor(underCursor, x, y, Gdx.input.isButtonPressed(Input.Buttons.LEFT));
                }
                draggedItemIcon.setVisible(false);
                draggedItemSlot = null;
                touchedItemSlot = null;
            }
        }
    }

    public void updateCursor(Actor source, float x, float y, boolean pressed) {
        if (hoveredItemSlot != null && source != hoveredItemSlot) {
            hoveredItemSlot.setColor(skin.getColor("default"));
        }
        if (source instanceof AbstractItemSlot) {
            hoveredItemSlot = (AbstractItemSlot)source;
            if (touchedItemSlot == null) {
                hoveredItemSlot.setColor(skin.getColor("light-grey"));
            } else {
                hoveredItemSlot.setColor(skin.getColor(pressed ? "grey" : "light-grey"));
            }
            if (hoveredItemSlot.getItem() != null) {
                itemTooltip.setItem(hoveredItemSlot);
                itemTooltip.setItemTooltipPosition(x, y);
                compareTooltip.setItem(hoveredItemSlot.getCompareItemSlot());
                compareTooltip.setCompareItemTooltipPosition(x, itemTooltip);
            } else {
                itemTooltip.clearIt(); // For fast item swap, when after swapping cursor is over empty item slot.
            }
        }
    }

    private boolean isOutOfDistance(Vector2 one, Vector2 two, float distance) {
        return (Math.abs(one.x - two.x) > distance || Math.abs(one.y - two.y) > distance);
    }

    private void dragItemSlotValidation(float x, float y) {
        Actor underCursor = parent.hit(x, y, true);
        if (underCursor instanceof AbstractItemSlot) {
            AbstractItemSlot itemSlot = ((AbstractItemSlot)underCursor);
            if (!itemSlot.isThisItemValidHere(draggedItemSlot.getItem())) {
                itemSlot.setColor(skin.getColor("red"));
            }
            // In case you drag an item from PaperDoll to an item that can't be placed on that slot in PaperDoll.
            if (!draggedItemSlot.isThisItemValidHere(itemSlot.getItem())) {
                draggedItemSlot.setColor(skin.getColor("red"));
                return;
            }
        }
        draggedItemSlot.setColor(skin.getColor("grey"));
    }
}

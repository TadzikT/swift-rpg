package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.math.GridPoint2;
import pl.swiftrpg.game.entities.items.Item;

// Stores last items sold to merchant. If all slots here are full, items in them will be moved by one item slot to make
// an empty slot.
public class MerchantBuyback extends AbstractItemSlotsContainer<MerchantBuybackItemSlot> {
    private int slotsInARow = 6;
    private int slotsInAColumn = 8;

    public MerchantBuyback(int slotSize, AbstractInventoryPanel parent, int itemSlotsAmount) {
        super(slotSize, parent);
        for (int i = 0; i < itemSlotsAmount; i++) {
            MerchantBuybackItemSlot merchantBuybackItemSlot = new MerchantBuybackItemSlot(calculateSlotPosition(i),
                    slotSize,this);
            getItemSlots().add(merchantBuybackItemSlot);
            addActor(merchantBuybackItemSlot);
        }
    }

    private GridPoint2 calculateSlotPosition(int n) {
        return new GridPoint2((getPadding() + ((n % slotsInARow) * getSlotSize())),
                (getSlotSize() * slotsInAColumn - (((n + slotsInARow) / slotsInARow) * getSlotSize())));
    }

    // Should be used only when selling an item to a merchant. Doesn't split StackableItems in more than one
    // item slot if it has quantity higher than maximum, like the base method, because when selling an item to a
    // merchant, StackableItem can't have quantity higher than maximum.
    @Override
    public boolean addItem(Item item) {
        for (AbstractItemSlot itemSlot : getItemSlots()) {
            if (itemSlot.getItem() == null) {
                itemSlot.setItem(item);
                return true;
            }
        }
        // If empty slot not founded moves all items in item slots by one item slot, deleting an item in first slot.
        order(0);
        getItemSlots().get(getItemSlots().size() - 1).setItem(item);
        return false;
    }

    public void order(int fromIndex) {
        for (int i = fromIndex; i < getItemSlots().size() - 1; i++) {
            getItemSlots().get(i).setItem(getItemSlots().get(i + 1).getItem());
        }
        getItemSlots().get(getItemSlots().size() - 1).setItem(null);
    }

}

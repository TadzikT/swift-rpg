package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.GridPoint2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.assets.File;
import pl.swiftrpg.game.entities.items.EquippableItem;
import pl.swiftrpg.game.entities.items.Item;
import pl.swiftrpg.game.entities.items.StackableItem;
import pl.swiftrpg.game.gui.GUI;
import pl.swiftrpg.game.utils.Utils;

public abstract class AbstractItemSlot extends Group {
    private Item item;
    private AbstractItemSlotsContainer parent;
    private Sprite backgroundSprite;
    private Sprite itemSprite;
    private Label itemQuantity = Assets.getFont().getLabel("", Consts.SMALL_FONT_SIZE);

    public AbstractItemSlot(GridPoint2 position, int slotSize, AbstractItemSlotsContainer parent) {
        this.backgroundSprite = setEmpty();
        this.parent = parent;
        setBounds(position.x, position.y, slotSize, slotSize);
        addActor(itemQuantity);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(backgroundSprite, getX(), getY(), getWidth(), getHeight());
        if (item != null) {
            batch.draw(itemSprite, getX(), getY(), getWidth(), getHeight());
        }
        super.draw(batch, parentAlpha);
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        if (!isThisItemValidHere(item)) {
            Utils.log("This item can't be put here.");
            return;
        }
        this.item = item;
        if (this.item == null) {
            this.backgroundSprite = setEmpty();
        } else {
            this.backgroundSprite = Assets.getAtlas(File.INVENTORY).createSprite(item.getItemRarity().getSlotTexture());
            if (item instanceof EquippableItem) {
                this.itemSprite = Assets.getAtlas(File.ITEMS).createSprite(((EquippableItem) (item)).getEquippableSlot().getName());
            } else {
                this.itemSprite = Assets.getAtlas(File.ITEMS).createSprite("Item");
            }
        }
        updateItemQuantityLabel();
    }

    /**
     * @param stackableItem can't have quantity more than maxQuantity.
     * @return how many quantity of an item can be moved from stackableItem to the item in this slot. 0 if items aren't
     * the same.
     */
    protected int quantityToMoveHere(StackableItem stackableItem) {
        if (!(item instanceof StackableItem && stackableItem.getName().equals(item.getName()))) {
            return 0;
        }
        StackableItem thisItem = ((StackableItem)item);
        if (thisItem.getQuantity() + stackableItem.getQuantity() <= StackableItem.getMaxQuantity()) {
            return stackableItem.getQuantity();
        } else {
            return StackableItem.getMaxQuantity() - thisItem.getQuantity();
        }
    }

    /**
     * @return stackableItem with remaining quantity of an item passed to the method, after the merge.
     * null if can't merge.
     */
    public StackableItem mergeItems(StackableItem stackableItem) {
        if (!(item instanceof StackableItem && stackableItem.getName().equals(item.getName()))) {
            return null;
        }
        int quantity = stackableItem.getQuantity() + ((StackableItem)item).getQuantity();
        int mergedQuantity = quantity;
        if (quantity > StackableItem.getMaxQuantity()) {
            mergedQuantity = StackableItem.getMaxQuantity();
        }
        ((StackableItem)item).setQuantity(mergedQuantity);
        updateItemQuantityLabel();
        stackableItem.setQuantity(quantity - mergedQuantity);
        return stackableItem;
    }

    private void updateItemQuantityLabel() {
        if (item instanceof StackableItem) {
            itemQuantity.setText(String.valueOf(((StackableItem) item).getQuantity()));
        } else {
            itemQuantity.setText("");
        }
        itemQuantity.setPosition(getWidth() - 14 - itemQuantity.getPrefWidth(), 22);
    }

    private Sprite setEmpty() {
        return Assets.getAtlas(File.INVENTORY).createSprite("slot-empty");
    }

    public boolean isThisItemValidHere(Item item) {
        return true;
    }



    public AbstractItemSlot getCompareItemSlot() {
        if (!(item instanceof EquippableItem)) {
            return null;
        }
        return GUI.getInstance().getInventorySystem().getInventory().getPaperDoll()
                .getPaperDollItemSlot(((EquippableItem) item).getEquippableSlot());
    }

    public abstract void swapItems(AbstractItemSlot sourceSlot);

    public AbstractItemSlotsContainer getContainer() {
        return parent;
    }

    public Sprite getBackgroundSprite() {
        return backgroundSprite;
    }

    public Sprite getItemSprite() {
        return itemSprite;
    }
}

package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.math.GridPoint2;

public class Equipment extends AbstractItemSlotsContainer<ItemSlot> {
    private int slotsInARow = 6;
    private int slotsInAColumn = 4;

    public Equipment(int slotSize, AbstractInventoryPanel parent, int itemSlotsAmount) {
        super(slotSize, parent);
        for (int i = 0; i < itemSlotsAmount; i++) {
            ItemSlot itemSlot = new ItemSlot(calculateSlotPosition(i), slotSize, this);
            getItemSlots().add(itemSlot);
            addActor(itemSlot);
        }
    }

    private GridPoint2 calculateSlotPosition(int n) {
        return new GridPoint2((getPadding() + ((n % slotsInARow) * getSlotSize())),
                (getSlotSize() * slotsInAColumn - (((n + slotsInARow) / slotsInARow) * getSlotSize())));
    }

}
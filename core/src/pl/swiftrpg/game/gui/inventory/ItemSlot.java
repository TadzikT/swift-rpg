package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.math.GridPoint2;
import pl.swiftrpg.game.entities.items.Item;
import pl.swiftrpg.game.entities.items.StackableItem;
import pl.swiftrpg.game.gui.GUI;

public class ItemSlot extends AbstractItemSlot {

    public ItemSlot(GridPoint2 position, int slotSize, AbstractItemSlotsContainer parent) {
        super(position, slotSize, parent);
    }

    @Override
    public void swapItems(AbstractItemSlot sourceSlot) {
        if (sourceSlot.getItem() == getItem()
                || !(isThisItemValidHere(sourceSlot.getItem()) && sourceSlot.isThisItemValidHere(getItem()))) {
            return;
        }

        Inventory inventory = GUI.getInstance().getInventorySystem().getInventory();
        // handling trade
        if (sourceSlot instanceof MerchantBuyItemSlot || sourceSlot instanceof MerchantBuybackItemSlot) {
            // handling trade to non-empty slot by merging StackableItem
            if (sourceSlot.getItem() instanceof StackableItem && getItem() != null) {
                StackableItem stackableItem = (StackableItem)sourceSlot.getItem();
                int quantityToBeMovedHere = quantityToMoveHere(stackableItem);
                if (quantityToBeMovedHere > 0 && inventory.takeGold(stackableItem
                        .getTotalPriceOf(quantityToBeMovedHere, sourceSlot.getContainer().getItemPriceMultiplier()))) {
                    StackableItem remainingItem = mergeItems((StackableItem) sourceSlot.getItem());
                    if (remainingItem != null) {
                        sourceSlot.setItem(remainingItem.getQuantity() > 0 ? remainingItem : null);
                    }
                }
            // handling trade to an empty slot (without merging)
            } else if (getItem() == null && inventory.takeGold(sourceSlot.getItem()
                    .getTotalPrice(sourceSlot.getContainer().getItemPriceMultiplier()))) {
                if (sourceSlot instanceof MerchantBuyItemSlot) {
                    setItem(sourceSlot.getItem());
                    sourceSlot.setItem(null);
                // handling trade to an empty slot (without merging) from MerchantBuybackItemSlot
                } else {
                    setItem(sourceSlot.getItem());
                    sourceSlot.setItem(null);
                    MerchantBuyback merchantBuyback = (MerchantBuyback) sourceSlot.getContainer();
                    merchantBuyback.order(merchantBuyback.getItemSlots().indexOf(sourceSlot));
                }
            }
        // handling non-trade StackableItem
        } else if (sourceSlot.getItem() instanceof StackableItem) {
            StackableItem remainingItem = mergeItems((StackableItem)sourceSlot.getItem());
            if (remainingItem != null) {
                sourceSlot.setItem(remainingItem.getQuantity() > 0 ? remainingItem : null);
            } else {
                Item tempItem = getItem();
                setItem(sourceSlot.getItem());
                sourceSlot.setItem(tempItem);
            }
        // handling non-trade non-StackableItem
        } else {
            Item tempItem = getItem();
            setItem(sourceSlot.getItem());
            sourceSlot.setItem(tempItem);
        }
    }

}
package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Group;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.assets.File;

public abstract class AbstractInventoryPanel extends Group {
    private Sprite background;

    public AbstractInventoryPanel() {
        setVisible(false);
        background = Assets.getAtlas(File.INVENTORY).createSprite("invbg");
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(getColor());
        batch.draw(background, getX(), getY(), getWidth(), getHeight());
        super.draw(batch, parentAlpha);
    }

}

package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Align;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.Game;
import pl.swiftrpg.game.entities.items.EquippableItemGenerator;
import pl.swiftrpg.game.entities.items.ItemRarity;
import pl.swiftrpg.game.entities.items.StackableItem;
import pl.swiftrpg.game.utils.Utils;

import java.util.Random;

public class Merchant extends AbstractInventoryPanel {
    private MerchantBuy merchantBuy;
    private MerchantBuyback merchantBuyback;
    private MerchantToggleButton merchantToggleButton;

    public Merchant(int slotSize) {
        super();
        setBounds(0, 0, 800, Consts.TARGET_HEIGHT);
        merchantBuy = new MerchantBuy(slotSize, this, 36);
        merchantBuyback = new MerchantBuyback(slotSize, this, 5);
        merchantBuyback.setVisible(false);
        merchantToggleButton = new MerchantToggleButton();
        addActor(merchantBuy);
        addActor(merchantBuyback);
        addActor(merchantToggleButton);

        merchantBuy.addItem(new StackableItem(ItemRarity.GREEN, "Cheese", 1, 10, 1));
        merchantBuy.addItem(new StackableItem(ItemRarity.GREEN, "Rat tail", 1, 10, 4));
        merchantBuy.addItem(new StackableItem(ItemRarity.GREEN, "Rat fur", 1, 10, 18));
        merchantBuy.addItem(new StackableItem(ItemRarity.GREEN, "Rat fur", 1, 10, 25));
        merchantBuy.addItem(new StackableItem(ItemRarity.BLUE, "Rat foot", 1, 10, 12));
        Random random = Utils.random;
        int itemCount = random.nextInt(30) + 5;
        for (int i = 0; i < itemCount; i++) {
            int itemLevel = random.nextInt(40) + 1;
            merchantBuy.addItem(EquippableItemGenerator.generateEquippableItem(itemLevel));
        }
    }

    public MerchantBuyback getMerchantBuyback() {
        return merchantBuyback;
    }

    public class MerchantToggleButton extends Button {
        public MerchantToggleButton() {
            super();
            setStyle(Game.getInstance().getSkin().get(Button.ButtonStyle.class));
            setBounds(Merchant.this.getX(Align.right) - 250, 0, 100, 100);
            addListener(new InputListener() {
                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    merchantBuy.setVisible(!merchantBuy.isVisible());
                    merchantBuyback.setVisible(!merchantBuyback.isVisible());
                    return true;
                }
            });
        }
    }
}


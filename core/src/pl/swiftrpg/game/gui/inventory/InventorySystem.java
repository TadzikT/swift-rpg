package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;
import java.util.List;

// This class will have Inventory, but also things like Stash and NPC trade panels.

public class InventorySystem extends Group {
    private Inventory inventory;
    private Stash stash;
    private List<Merchant> merchants = new ArrayList<>();
    private ItemTooltip itemTooltip;
    private ItemTooltip compareTooltip;
    private DraggedItemIcon draggedItemIcon;
    private int slotSize = 128;
    private InventorySystemListener inventorySystemListener;

    public InventorySystem() {
        inventory = new Inventory(slotSize);
        stash = new Stash(slotSize);
        itemTooltip = new ItemTooltip(false);
        compareTooltip = new ItemTooltip(true);
        draggedItemIcon = new DraggedItemIcon(slotSize);
        addActor(inventory);
        addActor(stash);
        addActor(itemTooltip);
        addActor(draggedItemIcon);
        addActor(compareTooltip);
        inventorySystemListener = new InventorySystemListener(this, itemTooltip, compareTooltip, draggedItemIcon);
        addListener(inventorySystemListener);
    }

    // Useful when you want to add an item to some slot and have that slot highlighted and item tooltip visible if slot
    // is currently under cursor.
    public void manualUpdateCursor() {
        Vector2 v = screenToLocalCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
        inventorySystemListener.updateCursor(hit(v.x, v.y, true), v.x, v.y,
                Gdx.input.isButtonPressed(Input.Buttons.LEFT));
    }

    public Inventory getInventory() {
        return inventory;
    }

    public Stash getStash() {
        return stash;
    }

    public Merchant newMerchant() {
        Merchant merchant = new Merchant(slotSize);
        merchants.add(merchant);
        addActor(merchant);
        merchant.toBack();
        return merchant;
    }
}

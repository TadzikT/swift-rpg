package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.math.GridPoint2;

public class MerchantBuy extends AbstractItemSlotsContainer<MerchantBuyItemSlot> {
    private int slotsInARow = 6;
    private int slotsInAColumn = 8;

    public MerchantBuy(int slotSize, AbstractInventoryPanel parent, int itemSlotsAmount) {
        super(slotSize, parent);
        setItemPriceMultiplier(3);
        for (int i = 0; i < itemSlotsAmount; i++) {
            MerchantBuyItemSlot merchantBuyItemSlot = new MerchantBuyItemSlot(calculateSlotPosition(i), slotSize,
                    this);
            getItemSlots().add(merchantBuyItemSlot);
            addActor(merchantBuyItemSlot);
        }
    }

    private GridPoint2 calculateSlotPosition(int n) {
        return new GridPoint2((getPadding() + ((n % slotsInARow) * getSlotSize())),
                (getSlotSize() * slotsInAColumn - (((n + slotsInARow) / slotsInARow) * getSlotSize())));
    }

}

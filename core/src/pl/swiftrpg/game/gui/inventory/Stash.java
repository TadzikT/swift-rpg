package pl.swiftrpg.game.gui.inventory;

import pl.swiftrpg.game.Consts;

public class Stash extends AbstractInventoryPanel {
    private StashEquipment stashEquipment;

    public Stash(int slotSize) {
        super();
        setBounds(0, 0, 800, Consts.TARGET_HEIGHT);
        stashEquipment = new StashEquipment(slotSize, this, 36);
        addActor(stashEquipment);
    }

}

package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.Game;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.entities.items.StackableItem;

public class DraggedItemIcon extends Group {
    private static Color color  = Game.getInstance().getSkin().getColor("default");
    private Sprite backgroundSprite;
    private Sprite itemSprite;
    private AbstractItemSlot itemSlot;
    private Label itemQuantity = Assets.getFont().getLabel("", Consts.SMALL_FONT_SIZE);

    public DraggedItemIcon(int slotSize) {
        setVisible(false);
        setTouchable(Touchable.disabled);
        setSize(slotSize * 0.7f, slotSize * 0.7f);
        addActor(itemQuantity);
    }

    public void setItemSlot(AbstractItemSlot itemSlot) {
        this.itemSlot = itemSlot;
        backgroundSprite = itemSlot.getBackgroundSprite();
        itemSprite = itemSlot.getItemSprite();
        if (itemSlot.getItem() instanceof StackableItem) {
            itemQuantity.setText(String.valueOf(((StackableItem)itemSlot.getItem()).getQuantity()));
        } else {
            itemQuantity.setText("");
        }
        itemQuantity.setPosition(getWidth() - 14 - itemQuantity.getPrefWidth(), 22);
        setVisible(true);
    }

    public void update(float x, float y) {
        setPosition(x, y);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(color);
        batch.draw(backgroundSprite, getX(), getY(), getWidth(), getHeight());
        batch.draw(itemSprite, getX(), getY(), getWidth(), getHeight());
        super.draw(batch, parentAlpha);
    }
}

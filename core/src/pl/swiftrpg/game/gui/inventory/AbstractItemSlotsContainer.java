package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.scenes.scene2d.Group;
import pl.swiftrpg.game.entities.items.Item;
import pl.swiftrpg.game.entities.items.StackableItem;
import pl.swiftrpg.game.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractItemSlotsContainer<T extends AbstractItemSlot> extends Group {
    private int slotSize;
    private AbstractInventoryPanel parent;
    private int padding = 16; // pixels on the left and right side of ItemSlot group
    private List<T> itemSlots = new ArrayList<>();
    private int itemPriceMultiplier = 1;

    public AbstractItemSlotsContainer(int slotSize, AbstractInventoryPanel parent) {
        this.slotSize = slotSize;
        this.parent = parent;
    }

    public boolean addItem(Item item) {
        if (item instanceof StackableItem) { // try merging item, if it's stackable
            for (AbstractItemSlot itemSlot : getItemSlots()) {
                StackableItem returnedItem = itemSlot.mergeItems((StackableItem)item);
                if (returnedItem != null && returnedItem.getQuantity() == 0) {
                    // Merge was successful. If it's fully merged we are done here. If not, loop for the next slot to
                    // find a place to merge remaining quantity of an item.
                    return true;
                }
            }
        }
        for (AbstractItemSlot itemSlot : getItemSlots()) {
            if (itemSlot.getItem() == null) {
                if (item instanceof StackableItem
                        && ((StackableItem) item).getQuantity() > StackableItem.getMaxQuantity()) {
                    StackableItem stackableItem = (StackableItem)item;
                    StackableItem itemToSet = stackableItem.takeSome(StackableItem.getMaxQuantity());
                    itemSlot.setItem(itemToSet);
                } else {
                    itemSlot.setItem(item);
                    return true;
                }
            }
        }
        Utils.log("It's full, can't add the item.");
        return false;
    }

    public int getSlotSize() {
        return slotSize;
    }

    public AbstractInventoryPanel getContainer() {
        return parent;
    }

    public int getPadding() {
        return padding;
    }

    public List<T> getItemSlots() {
        return itemSlots;
    }

    public int getItemPriceMultiplier() {
        return itemPriceMultiplier;
    }

    public void setItemPriceMultiplier(int itemPriceMultiplier) {
        this.itemPriceMultiplier = itemPriceMultiplier;
    }
}

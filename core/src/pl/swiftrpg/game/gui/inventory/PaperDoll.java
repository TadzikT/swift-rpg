package pl.swiftrpg.game.gui.inventory;

import com.badlogic.gdx.math.GridPoint2;
import pl.swiftrpg.game.entities.items.EquippableSlot;
import pl.swiftrpg.game.entities.items.Item;
import pl.swiftrpg.game.utils.Utils;

import java.util.EnumMap;
import java.util.Map;

public class PaperDoll extends AbstractItemSlotsContainer<PaperDollItemSlot> {

    public PaperDoll(int slotSize, AbstractInventoryPanel parent, int height) {
        super(slotSize, parent);
        Map<EquippableSlot, GridPoint2> positions = new EnumMap<>(EquippableSlot.class);
        positions.put(EquippableSlot.HEAD,      new GridPoint2(5, 1));
        positions.put(EquippableSlot.NECK,      new GridPoint2(6, 1));
        positions.put(EquippableSlot.HAND,      new GridPoint2(4, 2));
        positions.put(EquippableSlot.CHEST,     new GridPoint2(5, 2));
        positions.put(EquippableSlot.SHOULDERS, new GridPoint2(6, 2));
        positions.put(EquippableSlot.FINGER,    new GridPoint2(4, 3));
        positions.put(EquippableSlot.LEGS,      new GridPoint2(5, 3));
        positions.put(EquippableSlot.FEET,      new GridPoint2(5, 4));
        for (Map.Entry<EquippableSlot, GridPoint2> entry : positions.entrySet()) {
            PaperDollItemSlot paperDollItemSlot =
                    new PaperDollItemSlot(calculateSlotPosition(entry.getValue(), height), slotSize, this,
                            entry.getKey());
            getItemSlots().add(paperDollItemSlot);
            addActor(paperDollItemSlot);
        }
    }

    private GridPoint2 calculateSlotPosition(GridPoint2 position, int height) {
        return new GridPoint2((getPadding() + ((position.x - 1)  * getSlotSize())),
                (height - (getSlotSize() * position.y)));
    }

    public PaperDollItemSlot getPaperDollItemSlot(EquippableSlot equippableSlot) {
        for (PaperDollItemSlot paperDollItemSlot : getItemSlots()) {
            if (paperDollItemSlot.getEquippableSlot() == equippableSlot) {
                return paperDollItemSlot;
            }
        }
        return null;
    }

    @Override
    public boolean addItem(Item item) {
        Utils.log("You can't add an item to PaperDoll with this method.");
        return false;
    }
}

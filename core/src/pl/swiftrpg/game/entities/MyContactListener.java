package pl.swiftrpg.game.entities;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import pl.swiftrpg.game.entities.world.*;

public class MyContactListener implements ContactListener {

    @Override
    public void beginContact(Contact contact) {
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        Object entityA = contact.getFixtureA().getBody().getUserData();
        Object entityB = contact.getFixtureB().getBody().getUserData();

        if (entityA instanceof Bullet && entityB instanceof Enemy) {
            handle((Bullet)entityA, (Enemy)entityB, impulse.getNormalImpulses()[0], impulse.getNormalImpulses()[1]);
        } else if (entityA instanceof Enemy && entityB instanceof Bullet) {
            handle((Bullet)entityB, (Enemy)entityA, impulse.getNormalImpulses()[1], impulse.getNormalImpulses()[0]);
        }

        else if (entityA instanceof Enemy && entityB instanceof Player) {
            handle((Enemy)entityA, (Player)entityB, impulse.getNormalImpulses()[0], impulse.getNormalImpulses()[1]);
        } else if (entityA instanceof Player && entityB instanceof Enemy) {
            handle((Enemy)entityB, (Player)entityA, impulse.getNormalImpulses()[1], impulse.getNormalImpulses()[0]);
        }

        if (entityA instanceof Bullet && entityB instanceof EnemyShooter) {
            handle((Bullet)entityA, (EnemyShooter)entityB, impulse.getNormalImpulses()[0],
                    impulse.getNormalImpulses()[1]);
        } else if (entityA instanceof EnemyShooter && entityB instanceof Bullet) {
            handle((Bullet)entityB, (EnemyShooter)entityA, impulse.getNormalImpulses()[1],
                    impulse.getNormalImpulses()[0]);
        }

        else if (entityA instanceof EnemyShooter && entityB instanceof Player) {
            handle((EnemyShooter)entityA, (Player)entityB, impulse.getNormalImpulses()[0],
                    impulse.getNormalImpulses()[1]);
        } else if (entityA instanceof Player && entityB instanceof EnemyShooter) {
            handle((EnemyShooter)entityB, (Player)entityA, impulse.getNormalImpulses()[1],
                    impulse.getNormalImpulses()[0]);
        }

        else if (entityA instanceof EnemyShooterBullet && entityB instanceof Player) {
            handle((EnemyShooterBullet)entityA, (Player)entityB, impulse.getNormalImpulses()[0],
                    impulse.getNormalImpulses()[1]);
        } else if (entityA instanceof Player && entityB instanceof EnemyShooterBullet) {
            handle((EnemyShooterBullet)entityB, (Player)entityA, impulse.getNormalImpulses()[1],
                    impulse.getNormalImpulses()[0]);
        }
    }

    private void handle(Bullet bullet, Enemy enemy, float bulletImpulse, float enemyImpulse) {
        // not sure how both of this impulses work, usually only one of them is more than zero. let's just add them lol
        float impact = bulletImpulse + enemyImpulse;
        bullet.setLive(false);
        enemy.damage(impact);
    }

    private void handle(Enemy enemy, Player player, float enemyImpulse, float playerImpulse) {
        player.damage(10);
    }

    private void handle(Bullet bullet, EnemyShooter enemyShooter, float bulletImpulse, float enemyShooterImpulse) {
        // not sure how both of this impulses work, usually only one of them is more than zero. let's just add them lol
        float impact = bulletImpulse + enemyShooterImpulse;
        bullet.setLive(false);
        enemyShooter.damage(impact);
    }

    private void handle(EnemyShooter enemyShooter, Player player, float enemyShooterImpulse, float playerImpulse) {
        player.damage(10);
    }

    private void handle(EnemyShooterBullet enemyShooterBullet, Player player, float enemyShooterBulletImpulse,
                        float playerImpulse) {
        // not sure how both of this impulses work, usually only one of them is more than zero. let's just add them lol
        float impact = enemyShooterBulletImpulse + playerImpulse;
        enemyShooterBullet.setLive(false);
        player.damage(impact / 10);
    }

}

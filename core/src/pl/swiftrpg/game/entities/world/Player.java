package pl.swiftrpg.game.entities.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.assets.File;

public class Player extends Actor {
    private Sprite sprite = Assets.getAtlas(File.ENTITIES).createSprite("player");
    private World world;
    private Body body;
    private float halfWidth = 1, halfHeight = 1;
    private float velocity = 20;
    private boolean shooting = false;
    private float maxHp = 3000;
    private float hp = maxHp;
    private boolean live = true;

    public Player(World aWorld) {
        world = aWorld;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(0, 0);
        bodyDef.linearDamping = 3f;
        bodyDef.angularDamping = 0.7f;
        body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(halfWidth, halfHeight);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 5f;
        fixtureDef.friction = 0.3f;
        fixtureDef.restitution = 0.7f;
        body.createFixture(fixtureDef);
        body.setUserData(this);

        shape.dispose();
    }
    private void checkControls() {
        Vector2 pos = body.getPosition();
        Vector2 controls = new Vector2();
        if (Gdx.input.isKeyPressed(Keys.W)) {
            controls.y++;
        }
        if (Gdx.input.isKeyPressed(Keys.S)) {
            controls.y--;
        }
        if (Gdx.input.isKeyPressed(Keys.A)) {
            controls.x--;
        }
        if (Gdx.input.isKeyPressed(Keys.D)) {
            controls.x++;
        }
        body.applyLinearImpulse(controls.nor().scl(velocity), pos, true);
    }

    @Override
    public void act(float delta) {
        if (!live) {
            body.setTransform(new Vector2(0f, 0f), 0f);
            live = true;
        }
        checkControls();
        setBounds(Consts.BOX_TO_WORLD * (body.getPosition().x - halfWidth),
                Consts.BOX_TO_WORLD * (body.getPosition().y - halfHeight),
                Consts.BOX_TO_WORLD * halfWidth * 2,
                Consts.BOX_TO_WORLD * halfHeight * 2);
        setOrigin(Consts.BOX_TO_WORLD * halfWidth, Consts.BOX_TO_WORLD * halfHeight);
        setRotation(body.getAngle() *  MathUtils.radiansToDegrees);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(sprite, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), 1, 1,
                getRotation());
    }

    public Body getBody() {
        return body;
    }

    public boolean isShooting() {
        return shooting;
    }

    public void setShooting(boolean shooting) {
        this.shooting = shooting;
    }

    public float getHalfWidth() {
        return halfWidth;
    }

    public float getHalfHeight() {
        return halfHeight;
    }

    public float getMaxHp() {
        return maxHp;
    }

    public float getHp() {
        return hp;
    }

    public void damage(float hp) {
        this.hp -= hp;
        if (this.hp < 0) {
            this.hp = maxHp;
            live = false;
        }
    }
}

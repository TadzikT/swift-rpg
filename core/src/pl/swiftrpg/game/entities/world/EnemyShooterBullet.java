package pl.swiftrpg.game.entities.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.assets.File;
import pl.swiftrpg.game.entities.CollisionConsts;

import java.util.List;

public class EnemyShooterBullet extends Actor {
    private static Sprite sprite = Assets.getAtlas(File.ENTITIES).createSprite("enemyShooterBullet");
    private World world;
    private List<EnemyShooterBullet> enemyShooterBullets;
    private Body body;
    private float halfWidth = 0.09f, halfHeight = 0.22f;
    private float force = 400;
    private boolean live = true;

    public EnemyShooterBullet(World aWorld, List<EnemyShooterBullet> enemyShooterBullets, double angle,
                              EnemyShooter enemyShooter) {
        world = aWorld;
        this.enemyShooterBullets = enemyShooterBullets;

        Vector2 enemyShooterPos = enemyShooter.getBody().getPosition();
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(enemyShooterPos.x, enemyShooterPos.y));
        bodyDef.linearDamping = 0.3f;
        bodyDef.angularDamping = 0.7f;
        bodyDef.bullet = true;
        body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(halfWidth, halfHeight);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 100f;
        fixtureDef.friction = 0.3f;
        fixtureDef.restitution = 0.7f;

        fixtureDef.filter.groupIndex = CollisionConsts.BULLET; // comment this line to apply collision between bullets

        body.createFixture(fixtureDef);
        body.setTransform(body.getPosition().x + halfWidth + (float) (Math.cos(angle)
                        * (enemyShooter.getHalfWidth() * 1.5)), body.getPosition().y + halfHeight +
                        (float) (Math.sin(angle) * (enemyShooter.getHalfHeight() * 1.5)),
                (float) (angle - Math.PI / 2));
        body.setUserData(this);

        shape.dispose();

        body.applyLinearImpulse(new Vector2((float) Math.cos(angle), (float) Math.sin(angle)).scl(force),
                body.getPosition(), true);
    }

    @Override
    public void act(float delta) {
        if (!live) {
            remove();
        }
        setBounds(Consts.BOX_TO_WORLD * (body.getPosition().x - halfWidth),
                Consts.BOX_TO_WORLD * (body.getPosition().y - halfHeight),
                Consts.BOX_TO_WORLD * halfWidth * 2,
                Consts.BOX_TO_WORLD * halfHeight * 2);
        setOrigin(Consts.BOX_TO_WORLD * halfWidth, Consts.BOX_TO_WORLD * halfHeight);
        setRotation(body.getAngle() * MathUtils.radiansToDegrees);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(sprite, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), 1, 1,
                getRotation());
    }

    @Override
    public boolean remove() {
        world.destroyBody(body);
        enemyShooterBullets.remove(this);
        return super.remove();
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

}

package pl.swiftrpg.game.entities.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.Game;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.assets.File;
import pl.swiftrpg.game.world.WorldStage;

import java.util.List;

public class Merchant extends Actor {
    private static Sprite sprite = Assets.getAtlas(File.ENTITIES).createSprite("merchant");
    private WorldStage worldStage;
    private World world;
    private pl.swiftrpg.game.gui.inventory.Merchant merchant;
    private List<Merchant> merchants;
    private Body body;
    private float halfWidth = 1f, halfHeight = 1f;
    private boolean mouseOver = false;
    private boolean inInteractRange = false;
    private boolean inInteraction = false;

    public Merchant(WorldStage worldStage, World world, pl.swiftrpg.game.gui.inventory.Merchant merchant,
                    List<Merchant> merchants, float x, float y) {
        this.worldStage = worldStage;
        this.world = world;
        this.merchant = merchant;
        this.merchants = merchants;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        bodyDef.linearDamping = 3f;
        bodyDef.angularDamping = 0.7f;
        body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(halfWidth, halfHeight);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 10f;
        fixtureDef.friction = 0.3f;
        fixtureDef.restitution = 0.7f;
        body.createFixture(fixtureDef);
        body.setUserData(this);

        shape.dispose();

        addListener(new InputListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                mouseOver = true;
                worldStage.setMouseOveredActor(Merchant.this);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                mouseOver = false;
                worldStage.setMouseOveredActor(null);
            }
        });
    }

    @Override
    public void act(float delta) {
        if (inInteraction && !mouseOver) {
            setColor(Game.getInstance().getSkin().getColor("light-grey"));
        } else if (inInteraction) {
            Color lightGrey = Game.getInstance().getSkin().getColor("light-grey");
            Color green = Game.getInstance().getSkin().getColor("green");
            Color greenish = lightGrey.cpy().add(green);
            setColor(greenish);
        } else if (mouseOver && !inInteractRange) {
            setColor(Game.getInstance().getSkin().getColor("red"));
        } else if (mouseOver) {
            setColor(Game.getInstance().getSkin().getColor("green"));
        } else {
            setColor(Game.getInstance().getSkin().getColor("default"));
        }
        setBounds(Consts.BOX_TO_WORLD * (body.getPosition().x - halfWidth),
                Consts.BOX_TO_WORLD * (body.getPosition().y - halfHeight),
                Consts.BOX_TO_WORLD * halfWidth * 2,
                Consts.BOX_TO_WORLD * halfHeight * 2);
        setOrigin(Consts.BOX_TO_WORLD * halfWidth, Consts.BOX_TO_WORLD * halfHeight);
        setRotation(body.getAngle() * MathUtils.radiansToDegrees);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(sprite, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), 1, 1,
                getRotation());
    }

    @Override
    public boolean remove() {
        world.destroyBody(body);
        merchants.remove(this);
        return super.remove();
    }

    public boolean isMouseOver() {
        return mouseOver;
    }

    public Body getBody() {
        return body;
    }

    public boolean isInInteractRange() {
        return inInteractRange;
    }

    public void setInInteractRange(boolean inInteractRange) {
        this.inInteractRange = inInteractRange;
    }

    public boolean isInInteraction() {
        return inInteraction;
    }

    public void setInInteraction(boolean inInteraction) {
        this.inInteraction = inInteraction;
    }

    public pl.swiftrpg.game.gui.inventory.Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(pl.swiftrpg.game.gui.inventory.Merchant merchant) {
        this.merchant = merchant;
    }
}

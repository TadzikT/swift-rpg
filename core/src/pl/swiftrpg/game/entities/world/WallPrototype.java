package pl.swiftrpg.game.entities.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.assets.File;

public class WallPrototype extends Actor {
    private static NinePatch ninePatch = Assets.getAtlas(File.ENTITIES).createPatch("wallPrototype");
    private World world;
    private float startX, startY;

    public WallPrototype(World aWorld) {
        world = aWorld;
        setVisible(false);
        setSize(0, 0);
    }

    public void create(float x, float y) {
        setVisible(true);
        setPosition(x, y);
        startX = x;
        startY = y;
        toFront();
    }

    public Wall finish() {
        Wall wall = createWall();
        setVisible(false);
        setSize(0, 0);
        return wall;
    }

    private Wall createWall() {
        return new Wall(world, getX() / Consts.BOX_TO_WORLD, getY() / Consts.BOX_TO_WORLD,
                getWidth() / Consts.BOX_TO_WORLD, getHeight() / Consts.BOX_TO_WORLD, 0);
    }

    @Override
    public void act(float delta) {
        if (!isVisible()) return;
        Vector2 localPos = screenToLocalCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
        Vector2 pos = localToParentCoordinates(localPos);
        float x, y, width, height;
        if (pos.x < startX) {
            x = pos.x;
            width = startX - pos.x;
        } else {
            x = startX;
            width = pos.x - startX;
        }
        if (pos.y < startY) {
            y = pos.y;
            height = startY - pos.y;
        } else {
            y = startY;
            height = pos.y - startY;
        }
        setBounds(x, y, width, height);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        ninePatch.draw(batch, getX(), getY(), getWidth(), getHeight());
    }

}

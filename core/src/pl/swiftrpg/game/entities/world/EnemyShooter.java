package pl.swiftrpg.game.entities.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.assets.File;
import pl.swiftrpg.game.utils.Utils;
import pl.swiftrpg.game.world.WorldStage;

import java.util.List;

public class EnemyShooter extends Actor {
    private static Sprite sprite = Assets.getAtlas(File.ENTITIES).createSprite("enemyShooter");
    private WorldStage worldStage;
    private World world;
    private List<EnemyShooter> enemyShooters;
    private List<EnemyShooterBullet> enemyShooterBullets;
    private Player player;
    private Body body;
    private float halfWidth = 1.3f, halfHeight = 1.3f;
    private float velocity = 700;
    private float maxHp = 2000;
    private float hp = maxHp;
    private boolean live = true;
    private EnemyHealthBar enemyHealthBar;
    private float distanceToPlayerToKeep = 7f;
    private float distanceToPlayerWhenCanShoot = 10f;
    private int counter;

    public EnemyShooter(WorldStage worldStage, World aWorld, List<EnemyShooter> enemyShooters,
                        List<EnemyShooterBullet> enemyShooterBullets, EnemyHealthBar aEnemyHealthBar, Player aPlayer,
                        float x, float y) {
        this.worldStage = worldStage;
        world = aWorld;
        player = aPlayer;
        this.enemyShooters = enemyShooters;
        enemyHealthBar = aEnemyHealthBar;
        this.enemyShooterBullets = enemyShooterBullets;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        bodyDef.linearDamping = 3f;
        bodyDef.angularDamping = 0.7f;
        body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(halfWidth, halfHeight);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 7f;
        fixtureDef.friction = 0.3f;
        fixtureDef.restitution = 0.7f;
        body.createFixture(fixtureDef);
        body.setUserData(this);

        shape.dispose();
    }

    @Override
    public void act(float delta) {
        if (!live) {
            remove();
        }

        Vector2 deltaPos = body.getPosition().sub(player.getBody().getPosition());
        double angle = Math.atan2(deltaPos.y, deltaPos.x);
        Vector2 force = new Vector2((float)Math.cos(angle), (float)Math.sin(angle)).scl(velocity);
        if (deltaPos.len() > distanceToPlayerToKeep) {
            force.scl(-1);
        }
        body.applyForceToCenter(force,true);

        counter++;
        if (deltaPos.len() < distanceToPlayerWhenCanShoot && counter % 4 == 0) {
            EnemyShooterBullet enemyShooterBullet = new EnemyShooterBullet(world, enemyShooterBullets,
                    angle - Math.PI, this); // this "- Math.PI" is so weird. Player's Bullets didn't need that.
            enemyShooterBullets.add(enemyShooterBullet);
            worldStage.addActor(enemyShooterBullet);
        }

        setBounds(Consts.BOX_TO_WORLD * (body.getPosition().x - halfWidth),
                Consts.BOX_TO_WORLD * (body.getPosition().y - halfHeight),
                Consts.BOX_TO_WORLD * halfWidth * 2,
                Consts.BOX_TO_WORLD * halfHeight * 2);
        setOrigin(Consts.BOX_TO_WORLD * halfWidth, Consts.BOX_TO_WORLD * halfHeight);
        setRotation(body.getAngle() * MathUtils.radiansToDegrees);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(sprite, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), 1, 1,
                getRotation());
    }

    @Override
    public boolean remove() {
        world.destroyBody(body);
        enemyShooters.remove(this);
        enemyHealthBar.remove();
        if (Utils.random.nextFloat() < 0.8f) {
            worldStage.addItemDrop(body.getPosition());
        }
        return super.remove();
    }

    public float getMaxHp() {
        return maxHp;
    }

    public float getHp() {
        return hp;
    }

    public void damage(float hp) {
        this.hp -= hp;
        if (this.hp < 0) {
            live = false;
        }
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public Body getBody() {
        return body;
    }

    public float getHalfWidth() {
        return halfWidth;
    }

    public float getHalfHeight() {
        return halfHeight;
    }
}


package pl.swiftrpg.game.entities.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import pl.swiftrpg.game.Game;
import pl.swiftrpg.game.utils.Utils;

import java.util.List;

public class EnemyHealthBar extends Actor {
    private static Texture healthBar = Utils.createTexture(Game.getInstance().getSkin().getColor("red"));
    private static Texture healthBarBackground =
            Utils.createTexture(Game.getInstance().getSkin().getColor("blackish"));
    private static float healthBarBorder = 3;
    private List<EnemyHealthBar> enemyHealthBars;
    private Enemy enemy;
    private EnemyShooter enemyShooter;

    public EnemyHealthBar(List<EnemyHealthBar> enemyHealthBars) {
        this.enemyHealthBars = enemyHealthBars;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    public void setEnemyShooter(EnemyShooter enemyShooter) {
        this.enemyShooter = enemyShooter;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        if (enemyShooter == null) {
            drawEnemy(batch);
        } else {
            drawEnemyShooter(batch);
        }

    }

    private void drawEnemyShooter(Batch batch) {
        float healthBarWidth = Utils.map(enemyShooter.getHp(), 0, enemyShooter.getMaxHp(),0,
                enemyShooter.getWidth() - healthBarBorder * 2);
        batch.draw(healthBarBackground, enemyShooter.getX(), enemyShooter.getY() + enemyShooter.getHeight() + 20
                        - healthBarBorder, enemyShooter.getWidth(),10 + healthBarBorder * 2);
        batch.draw(healthBar, enemyShooter.getX() + healthBarBorder, enemyShooter.getY()
                        + enemyShooter.getHeight() + 20, healthBarWidth, 10);
    }

    private void drawEnemy(Batch batch) {
        float healthBarWidth = Utils.map(enemy.getHp(), 0, enemy.getMaxHp(),0,
                enemy.getWidth() - healthBarBorder * 2);
        batch.draw(healthBarBackground, enemy.getX(), enemy.getY() + enemy.getHeight() + 20 - healthBarBorder,
                enemy.getWidth(),10 + healthBarBorder * 2);
        batch.draw(healthBar, enemy.getX() + healthBarBorder, enemy.getY()  + enemy.getHeight() + 20,
                healthBarWidth,10);
    }

    @Override
    public boolean remove() {
        enemyHealthBars.remove(this);
        return super.remove();
    }
}

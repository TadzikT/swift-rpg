package pl.swiftrpg.game.entities.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.assets.Assets;
import pl.swiftrpg.game.assets.File;

public class Wall extends Actor {
    private static NinePatch ninePatch = Assets.getAtlas(File.ENTITIES).createPatch("wall");
    private World world;
    private Body body;
    private float halfWidth, halfHeight;
    private boolean rotated = false;

    public Wall(World aWorld, float x, float y, float width, float height, float angle) {
        world = aWorld;

        halfWidth = width / 2;
        halfHeight = height / 2;

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(new Vector2(x, y));
        bodyDef.type = BodyDef.BodyType.KinematicBody;
        body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(halfWidth, halfHeight);
        body.setTransform(x + halfWidth, y + halfHeight, (float)Math.toRadians(angle));

        body.createFixture(shape, 0.0f);
        body.setUserData(this);
        shape.dispose();
    }

    @Override
    public void act(float delta) {
        setBounds(Consts.BOX_TO_WORLD * (body.getPosition().x - halfWidth),
                Consts.BOX_TO_WORLD * (body.getPosition().y - halfHeight),
                Consts.BOX_TO_WORLD * halfWidth * 2,
                Consts.BOX_TO_WORLD * halfHeight * 2);
        setOrigin(Consts.BOX_TO_WORLD * halfWidth, Consts.BOX_TO_WORLD * halfHeight);
        setRotation(body.getAngle() *  MathUtils.radiansToDegrees);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        ninePatch.draw(batch, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), 1, 1,
                getRotation());
    }

    public void rotate(boolean direction) {
        body.setAngularVelocity(direction ? 2 : -2);
    }

    public void stopRotating() {
        body.setAngularVelocity(0);
    }

    @Override
    public boolean remove() {
        world.destroyBody(body);
        return super.remove();
    }

}

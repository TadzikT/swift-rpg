package pl.swiftrpg.game.entities.items;

public enum Attribute {
    VITALITY("Vitality"),
    // MANA("Mana"), nie wiem jak to nazwac, ma zwiekszac max many
    DAMAGE("Damage"),
    ARMOR("Armor"),
    LIFESTEAL("Lifesteal"),
    MANA_REGENERATION("Mana regeneration"),
    REFLECTED_DAMAGE("Reflected damage"), //część otrzymywanych obrażeń od moba wraca do atakującego
    CRITICAL_HIT_CHANCE("Critical hit chance"),
    STUN_HIT_CHANCE("Stunning hit chance"),
    BLEEDING_HIT_CHANCE("Bleeding hit chance"), //DoT (damage over time)
    SPLASH_HIT_DAMAGE("Splash hit chance"); //AoE (area of effect)

    private final String name;

    public String getName() {
        return name;
    }

    Attribute(String name) {
        this.name = name;
    }
}


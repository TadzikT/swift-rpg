package pl.swiftrpg.game.entities.items;

public enum EquippableSlot {
    HEAD("Head"),
    NECK("Neck"),
    HAND("Hand"),
    CHEST("Chest"),
    SHOULDERS("Shoulders"),
    FINGER("Finger"),
    LEGS("Legs"),
    FEET("Feet");

    private final String name;

    EquippableSlot(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

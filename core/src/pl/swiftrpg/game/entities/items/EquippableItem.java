package pl.swiftrpg.game.entities.items;

import java.util.Map;

public class EquippableItem extends Item {
    private EquippableSlot equippableSlot;
    private Map<Attribute, Integer> attributes;

    public EquippableItem(ItemRarity itemRarity, String name, int level, int price, EquippableSlot equippableSlot,
                          Map<Attribute, Integer> attributes) {
        super(itemRarity, name, level, price);
        this.equippableSlot = equippableSlot;
        this.attributes = attributes;
    }

    public EquippableSlot getEquippableSlot() {
        return equippableSlot;
    }

    public Map<Attribute, Integer> getAttributes() {
        return attributes;
    }
}

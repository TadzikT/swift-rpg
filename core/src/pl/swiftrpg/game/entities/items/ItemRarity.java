package pl.swiftrpg.game.entities.items;

public enum ItemRarity {
    WHITE("slot-white", "white"),
    GREEN("slot-green", "green"),
    BLUE("slot-blue", "blue"),
    PURPLE("slot-purple", "purple");

    private final String slotTexture;
    private final String color;

    public String getSlotTexture() {
        return slotTexture;
    }

    public String getColor() {
        return color;
    }

    ItemRarity(String slotTexture, String color) {
        this.slotTexture = slotTexture;
        this.color = color;
    }
}

package pl.swiftrpg.game.entities.items;

public class Item {
    private ItemRarity itemRarity;
    private String name;
    private int level;
    private int price;

    public Item(ItemRarity itemRarity, String name, int level, int price) {
        this.itemRarity = itemRarity;
        this.name = name;
        this.level = level;
        this.price = price;
    }

    public ItemRarity getItemRarity() {
        return itemRarity;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public int getPrice() {
        return price;
    }

    // takes in account item quantity
    public int getRealPrice() {
        return price;
    }

    // takes into account item quantity and container's item price multiplier
    public int getTotalPrice(int priceMultiplier) {
        return getRealPrice() * priceMultiplier;
    }
}

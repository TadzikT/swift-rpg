package pl.swiftrpg.game.entities.items;

import pl.swiftrpg.game.utils.Point2D;

import java.util.*;

public class EquippableItemGenerator {
    private static Random random = new Random();

    public static EquippableItem generateEquippableItem(int level) {
        ItemRarity itemRarity = randomRarity();
        String name = randomName();
        EquippableSlot equippableSlot = randomEquippableSlot();
        Point2D attributeValueRange = getAttributeValueRange(level, itemRarity);
        int attributesAmount = randomAttributesAmount(itemRarity);
        Map<Attribute, Integer> attributes = randomAttributes(attributeValueRange, attributesAmount);
        int price = getPrice(attributes);
        return new EquippableItem(itemRarity, name, level, price, equippableSlot, attributes);
    }

    private static ItemRarity randomRarity() {
        int randomNumber = random.nextInt(100);
        if (randomNumber < 60) {
            return ItemRarity.WHITE;
        } else if (randomNumber < 90) {
            return ItemRarity.GREEN;
        } else if (randomNumber < 99) {
            return ItemRarity.BLUE;
        } else {
            return ItemRarity.PURPLE;
        }
    }

    private static String randomName() {
        return "Nice thing";
    }

    private static EquippableSlot randomEquippableSlot() {
        return EquippableSlot.values()[random.nextInt(EquippableSlot.values().length)];
    }

    private static Point2D getAttributeValueRange(int level, ItemRarity itemRarity) {
        int baseValue = level * 5;
        switch (itemRarity){
            case WHITE:     return new Point2D((int)(baseValue * 0.5), baseValue);
            case GREEN:     return new Point2D(baseValue, baseValue * 2);
            case BLUE:      return new Point2D(baseValue * 2, baseValue * 4);
            case PURPLE:    return new Point2D(baseValue * 4, baseValue * 8);
        }
        return null;
    }
    private static int randomAttributesAmount(ItemRarity itemRarity) {
        switch (itemRarity) {
            case WHITE:     return random.nextInt((2 - 1) + 1) + 1; // 1 - 2 attributes
            case GREEN:     return random.nextInt((3 - 2) + 1) + 2; // 2 - 3 attributes
            case BLUE:      return random.nextInt((5 - 3) + 1) + 3; // 3 - 5 attributes
            case PURPLE:    return random.nextInt((7 - 5) + 1) + 5; // 5 - 7 attributes
        }
        return -1;
    }

    private static Map<Attribute,Integer> randomAttributes(Point2D attributeValueRange, int attributesAmount) {
        Map<Attribute, Integer> attributes = new HashMap<>();
        Set<Attribute> set = new HashSet<>();
        for (int i = 0; i < attributesAmount; ++i) {
            Attribute key;
            // This "do while" makes sure that every attribute to be added is unique.
            do {
                key = Attribute.values()[random.nextInt(Attribute.values().length)];
            }
            while (set.contains(key));
            set.add(key);
        }
        for (Attribute type: set) {
            attributes.put(type, random.nextInt((attributeValueRange.y - attributeValueRange.x) + 1)
                    + attributeValueRange.x);
        }
        return attributes;
    }

    private static int getPrice(Map<Attribute, Integer> attributes) {
        int price = 0;
        for (Integer value : attributes.values()) {
            price += value;
        }
        return price;
    }

}

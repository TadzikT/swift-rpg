package pl.swiftrpg.game.entities.items;

public class StackableItem extends Item {
    static private int maxQuantity = 20;
    private int quantity;

    public StackableItem(ItemRarity itemRarity, String name, int level, int price, int quantity) {
        super(itemRarity, name, level, price);
        this.quantity = quantity;
    }

    @Override
    public int getRealPrice() {
        return getPrice() * quantity;
    }

    public int getTotalPriceOf(int quantity, int priceMultiplier) {
        return getPrice() * quantity * priceMultiplier;
    }

    public StackableItem takeSome(int quantity) {
        this.quantity -= quantity;
        return new StackableItem(getItemRarity(), getName(), getLevel(), getPrice(), quantity);
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public static int getMaxQuantity() {
        return maxQuantity;
    }
}

package pl.swiftrpg.game.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

import java.util.Random;

public class Utils {

    public static final Random random = new Random();

    public static void log(String text) {
        try {
            Class clazz = Class.forName(Thread.currentThread().getStackTrace()[2].getClassName());
            Gdx.app.log(clazz.getSimpleName(), text);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static float map(float valueCoord1, float startCoord1, float endCoord1, float startCoord2,
                            float endCoord2) {
        final float EPSILON = 1e-12f;
        if (Math.abs(endCoord1 - startCoord1) < EPSILON) {
            throw new ArithmeticException("/ 0");
        }
        float ratio = (endCoord2 - startCoord2) / (endCoord1 - startCoord1);
        return ratio * (valueCoord1 - startCoord1) + startCoord2;
    }

    //useful for drawing filled rectangles, for instance on health bars
    public static Texture createTexture(Color color) {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(color);
        pixmap.fillRectangle(0, 0, 1, 1);
        Texture texture = new Texture(pixmap);
        pixmap.dispose();
        return texture;
    }
}

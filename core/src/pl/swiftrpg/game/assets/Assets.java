package pl.swiftrpg.game.assets;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import java.util.ArrayList;
import java.util.List;

public class Assets {

    private static AssetManager assetManager = new AssetManager();
    private static LazyBitmapFontController font = new LazyBitmapFontController();

    public static void loadAssets() {
        for (File file : File.values()) {
            assetManager.load(file.getPath(), file.getType());
        }
    }

    public static TextureAtlas getAtlas(File file) {
        return assetManager.get(file.getPath(), TextureAtlas.class);
    }

    public static <T> T get(File file) {
        return (T) file.getType().cast(assetManager.get(file.getPath(), file.getType()));
    }

    public static File[] getAll(Class type) {
        List<File> list = new ArrayList<>();
        for (File f : File.values()) {
            if (f.getType().equals(type)) {
                list.add(f);
            }
        }
        return list.toArray(new File[list.size()]);
    }

    public static AssetManager getManager() {
        return assetManager;
    }

    public static LazyBitmapFontController getFont() {
        return font;
    }
}

package pl.swiftrpg.game.assets;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public enum File {
    GUI("gui.atlas", TextureAtlas.class),
    INVENTORY("inventory.atlas", TextureAtlas.class),
    ENTITIES("entities.atlas", TextureAtlas.class),
    ITEMS("items.atlas", TextureAtlas.class);

    private String path;
    private Class type;

    File(final String path, final Class type) {
        this.path = path;
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public Class getType() {
        return type;
    }
}

package pl.swiftrpg.game.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.Game;
import pl.swiftrpg.game.entities.MyContactListener;
import pl.swiftrpg.game.entities.items.EquippableItem;
import pl.swiftrpg.game.entities.items.EquippableItemGenerator;
import pl.swiftrpg.game.entities.world.*;
import pl.swiftrpg.game.gui.GUI;
import pl.swiftrpg.game.gui.inventory.Equipment;

import java.util.ArrayList;
import java.util.List;

public class WorldStage extends Stage {
    private World world = new World(new Vector2(0, 0), true);
    private Player player;
    private WallPrototype wallPrototype;
    private List<Wall> walls = new ArrayList<>();
    private List<Bullet> bullets = new ArrayList<>();
    private List<Enemy> enemies = new ArrayList<>();
    private List<EnemyHealthBar> enemyHealthBars = new ArrayList<>();
    private List<EnemyShooter> enemyShooters = new ArrayList<>();
    private List<EnemyShooterBullet> enemyShooterBullets = new ArrayList<>();
    private List<Stash> stashes = new ArrayList<>();
    private List<Merchant> merchants = new ArrayList<>();
    private List<ItemDrop> itemDrops = new ArrayList<>();
    private Wall rotatedWall;
    private Actor mouseOveredActor;
    private Actor interactedActor;
    private float interactRange = 5;

    public WorldStage(Viewport viewport, SpriteBatch batch) {
        super(viewport, batch);
        world.setContactListener(new MyContactListener());
        player = new Player(world);
        wallPrototype = new WallPrototype(world);
        addActor(player);
        addActor(wallPrototype);
        walls.add(new Wall(world, -10, -10, 20, 2, 40));
        walls.add(new Wall(world, 6, 2, 10, 8, 120));
        walls.add(new Wall(world, -30, 6, 54, 3, 4));
        walls.add(new Wall(world, -8, 2, 6, 2, 0));
        walls.add(new Wall(world, -19, -4, 4, 18, 0));
        for (Wall wall : walls) {
            addActor(wall);
        }
        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Buttons.MIDDLE) {
                    Game.getInstance().getWorldCamera().zoom = 1;
                } else if (button == Buttons.LEFT) {
                    wallPrototype.create(x, y);
                } else if (button == Buttons.RIGHT) {
                    player.setShooting(true);
                }
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Buttons.LEFT) {
                    Wall wall = wallPrototype.finish();
                    walls.add(wall);
                    addActor(wall);
                } else if (button == Buttons.RIGHT) {
                    player.setShooting(false);
                }
            }

            @Override
            public boolean scrolled(InputEvent event, float x, float y, int amount) {
                Game.getInstance().getWorldCamera().zoom *= (amount == -1 ? 0.9f : 10 / 9f);
                return true;
            }

            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                switch (keycode) {
                    case Input.Keys.CONTROL_LEFT: {
                        Vector2 pos = screenToStageCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
                        Actor actor = hit(pos.x, pos.y, false);
                        if (actor instanceof Wall) {
                            actor.remove();
                            walls.remove(actor);
                        }
                        break;
                    }
                    case Input.Keys.Z: {
                        rotateWall(true);
                        break;
                    }
                    case Input.Keys.X: {
                        rotateWall(false);
                        break;
                    }
                    case Input.Keys.C: {
                        Vector2 pos = screenToStageCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
                        pos.x /= Consts.BOX_TO_WORLD;
                        pos.y /= Consts.BOX_TO_WORLD;
                        EnemyHealthBar enemyHealthBar = new EnemyHealthBar(enemyHealthBars);
                        Enemy enemy = new Enemy(WorldStage.this, world, enemies, enemyHealthBar, player, pos.x, pos.y);
                        enemyHealthBars.add(enemyHealthBar);
                        enemies.add(enemy);
                        enemyHealthBar.setEnemy(enemy);
                        addActor(enemyHealthBar);
                        addActor(enemy);
                        enemy.toBack(); //so it will be under all health bars
                        break;
                    }
                    case Input.Keys.F: {
                        Vector2 pos = screenToStageCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
                        pos.x /= Consts.BOX_TO_WORLD;
                        pos.y /= Consts.BOX_TO_WORLD;
                        EnemyHealthBar enemyHealthBar = new EnemyHealthBar(enemyHealthBars);
                        EnemyShooter enemyShooter = new EnemyShooter(WorldStage.this, world, enemyShooters,
                                enemyShooterBullets, enemyHealthBar, player, pos.x, pos.y);
                        enemyHealthBars.add(enemyHealthBar);
                        enemyShooters.add(enemyShooter);
                        enemyHealthBar.setEnemyShooter(enemyShooter);
                        addActor(enemyHealthBar);
                        addActor(enemyShooter);
                        enemyShooter.toBack(); //so it will be under all health bars
                        break;
                    }
                    case Input.Keys.V: {
                        Vector2 pos = screenToStageCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
                        pos.x /= Consts.BOX_TO_WORLD;
                        pos.y /= Consts.BOX_TO_WORLD;
                        Stash stash = new Stash(WorldStage.this, world, player, stashes, pos.x, pos.y);
                        stashes.add(stash);
                        addActor(stash);
                        stash.toBack();
                        break;
                    }
                    case Input.Keys.B: {
                        Vector2 pos = screenToStageCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
                        pos.x /= Consts.BOX_TO_WORLD;
                        pos.y /= Consts.BOX_TO_WORLD;
                        pl.swiftrpg.game.gui.inventory.Merchant merchantUI
                                = GUI.getInstance().getInventorySystem().newMerchant();
                        Merchant merchant = new Merchant(WorldStage.this, world, merchantUI, merchants, pos.x, pos.y);
                        merchants.add(merchant);
                        addActor(merchant);
                        merchant.toBack();
                        break;
                    }
                    case Input.Keys.N: {
                        Vector2 pos = screenToStageCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
                        pos.x /= Consts.BOX_TO_WORLD;
                        pos.y /= Consts.BOX_TO_WORLD;
                        addItemDrop(pos);
                        break;
                    }
                    case Input.Keys.Q: {
                        if (mouseOveredActor != null) {
                            if (mouseOveredActor instanceof Stash && ((Stash) mouseOveredActor).isInInteractRange()) {
                                pl.swiftrpg.game.gui.inventory.Stash stashUI
                                        = GUI.getInstance().getInventorySystem().getStash();
                                if (mouseOveredActor == interactedActor) {
                                    stashUI.setVisible(false);
                                    ((Stash)interactedActor).setInInteraction(false);
                                    interactedActor = null;
                                    break;
                                } else if (interactedActor != null) {
                                    if (interactedActor instanceof Merchant) {
                                        Merchant merchant = (Merchant) interactedActor;
                                        merchant.getMerchant().setVisible(false);
                                        merchant.setInInteraction(false);
                                        interactedActor = null;
                                    } else if (interactedActor instanceof Stash) {
                                        Stash stash = (Stash) interactedActor;
                                        stashUI.setVisible(false);
                                        stash.setInInteraction(false);
                                        interactedActor = null;
                                    }
                                }
                                stashUI.setVisible(true);
                                interactedActor = mouseOveredActor;
                                ((Stash)interactedActor).setInInteraction(true);
                            } else if (mouseOveredActor instanceof Merchant
                                    && ((Merchant) mouseOveredActor).isInInteractRange()) {
                                if (mouseOveredActor == interactedActor) {
                                    ((Merchant) mouseOveredActor).getMerchant().setVisible(false);
                                    ((Merchant)interactedActor).setInInteraction(false);
                                    interactedActor = null;
                                    break;
                                } else if (interactedActor != null) {
                                    if (interactedActor instanceof Merchant) {
                                        Merchant merchant = (Merchant) interactedActor;
                                        merchant.getMerchant().setVisible(false);
                                        merchant.setInInteraction(false);
                                        interactedActor = null;
                                    } else if (interactedActor instanceof Stash) {
                                        Stash stash = (Stash) interactedActor;
                                        GUI.getInstance().getInventorySystem().getStash().setVisible(false);
                                        stash.setInInteraction(false);
                                        interactedActor = null;
                                    }
                                }
                                ((Merchant) mouseOveredActor).getMerchant().setVisible(true);
                                interactedActor = mouseOveredActor;
                                ((Merchant)interactedActor).setInInteraction(true);
                            } else if (mouseOveredActor instanceof ItemDrop
                                    && ((ItemDrop) mouseOveredActor).isInInteractRange()) {
                                Equipment equipment = GUI.getInstance().getInventorySystem().getInventory()
                                        .getEquipment();
                                boolean isSuccessful = equipment.addItem(((ItemDrop) mouseOveredActor).getItem());
                                // set to false only if item was added successfully to the equipment.
                                ((ItemDrop) mouseOveredActor).setLive(!isSuccessful);

                            }
                        }
                        break;
                    }
                }
                return false;
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                switch (keycode) {
                    case Input.Keys.Z: {
                        if (rotatedWall != null) {
                            rotatedWall.stopRotating();
                        }
                        break;
                    }
                    case Input.Keys.X: {
                        if (rotatedWall != null) {
                            rotatedWall.stopRotating();
                        }
                        break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void act(float delta) {
        getCamera().position.x = Consts.BOX_TO_WORLD * player.getBody().getPosition().x;
        getCamera().position.y = Consts.BOX_TO_WORLD * player.getBody().getPosition().y;
        if (player.isShooting()) {
            Vector2 pos = screenToStageCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
            pos.x /= Consts.BOX_TO_WORLD;
            pos.y /= Consts.BOX_TO_WORLD;
            pos.sub(player.getBody().getPosition());
            double angle = Math.atan2(pos.y, pos.x);
            Bullet bullet = new Bullet(world, bullets, angle, player);
            bullets.add(bullet);
            addActor(bullet);
        }
        if (mouseOveredActor != null) {
            if (mouseOveredActor instanceof Stash) {
                Stash stash = (Stash) mouseOveredActor;
                if (stash.getBody().getPosition().sub(player.getBody().getPosition()).len() < interactRange) {
                    stash.setInInteractRange(true);
                } else {
                    stash.setInInteractRange(false);
                }
            } else if (mouseOveredActor instanceof Merchant) {
                Merchant merchant = (Merchant) mouseOveredActor;
                if (merchant.getBody().getPosition().sub(player.getBody().getPosition()).len() < interactRange) {
                    merchant.setInInteractRange(true);
                } else {
                    merchant.setInInteractRange(false);
                }
            } else if (mouseOveredActor instanceof ItemDrop) {
                ItemDrop itemDrop = (ItemDrop) mouseOveredActor;
                if (itemDrop.getBody().getPosition().sub(player.getBody().getPosition()).len() < interactRange) {
                    itemDrop.setInInteractRange(true);
                } else {
                    itemDrop.setInInteractRange(false);
                }
            }
        }
        if (interactedActor != null) {
            if (interactedActor instanceof Stash) {
                Stash stash = (Stash) interactedActor;
                if (stash.getBody().getPosition().sub(player.getBody().getPosition()).len() > interactRange) {
                    GUI.getInstance().getInventorySystem().getStash().setVisible(false);
                    stash.setInInteraction(false);
                    interactedActor = null;
                }
            } else if (interactedActor instanceof Merchant) {
                Merchant merchant = (Merchant) interactedActor;
                if (merchant.getBody().getPosition().sub(player.getBody().getPosition()).len() > interactRange) {
                    merchant.getMerchant().setVisible(false);
                    merchant.setInInteraction(false);
                    interactedActor = null;
                }
            }
        }
        world.step(delta, 8, 3);
        super.act(delta);
    }

    private void rotateWall(boolean direction) {
        Vector2 pos = screenToStageCoordinates(new Vector2(Gdx.input.getX(), Gdx.input.getY()));
        Actor actor = hit(pos.x, pos.y, false);
        if (actor instanceof Wall) {
            rotatedWall = (Wall)actor;
            rotatedWall.rotate(direction);
        }
    }

    public void addItemDrop(Vector2 pos) {
        EquippableItem item = EquippableItemGenerator.generateEquippableItem(1);
        ItemDrop itemDrop = new ItemDrop(WorldStage.this, world, item, itemDrops, pos.x, pos.y);
        itemDrops.add(itemDrop);
        addActor(itemDrop);
        itemDrop.toBack();
    }

    public Player getPlayer() {
        return player;
    }

    public Actor getMouseOveredActor() {
        return mouseOveredActor;
    }

    public void setMouseOveredActor(Actor mouseOveredActor) {
        this.mouseOveredActor = mouseOveredActor;
    }
}

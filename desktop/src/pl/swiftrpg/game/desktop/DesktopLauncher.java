package pl.swiftrpg.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import pl.swiftrpg.game.Consts;
import pl.swiftrpg.game.Game;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Consts.WINDOWED_WIDTH;
		config.height = Consts.WINDOWED_HEIGHT;
		config.title = Consts.GAME_NAME;
        config.samples = Consts.MSAA_SAMPLES;
		// config.addIcon(Consts.DESKTOP_ICON_PATH, Files.FileType.Internal);
		new LwjglApplication(Game.getInstance(), config);
	}
}
